import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Get the email',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Get the email'),
        ),
        body: Center(
          child: TestFetch(),
        ),
      ),
    );
  }
}

class TestFetch extends StatelessWidget {
  static final DatabaseReference  databaseRef = FirebaseDatabase.instance.reference().child("test");
  final _fetchData = databaseRef.once();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _fetchData ,
        builder: (context, snap) {
          if (snap.hasError) {
            return Text("Error: ${snap.error}");
          }
          if (snap.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicator();
          }
          DataSnapshot data = snap.data as DataSnapshot;
          return Text("Data from server: ${data.value}");
        });

  }
}

